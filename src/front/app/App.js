import React from 'react';

import {Router, Route, Switch, Redirect} from 'react-router-dom';

import {PrivateRoute} from "./components/Router/PrivateRoute";

const App = ({routes, history}) => {
  return (
    <Router history={history}>
      <Switch>
        {routes && routes.map(route => {
          const RouteComponent = route.access === 'logged' ? PrivateRoute : Route;

          return <RouteComponent
            key={route.path}
            path={route.path}
            {...route.config}
            render={props => <route.Component {...props} routes={route.routes}/>}/>
        })}

        <Route render={() => <Redirect to={routes[0].path}/>}/>
      </Switch>
    </Router>
  );
};

export default App;

