import EmployeesRoute from "./features/base/employee/EmployeesRoute";
import EmployeeAddRoute from "./features/base/employee/EmployeeAddRoute";
import LoginRoute from "./features/login/LoginRoute";

import BasicPage from './features/base/BaseRoute';

export const ACCESS = { logged: 'logged', def: 'default' };

export const loginPage = {
  path: '/login',
  access: ACCESS.def,
  Component: LoginRoute,
};

export const employeeListRoute = {
  access: ACCESS.logged,
  path: '/basic/employees',
  routeParams: {
    search: '?count=15'
  },
  Component: EmployeesRoute,
  config: {
    exact: true,
  }
};

export const employeeAddRoute = {
  path: '/basic/employees/add',
  access: ACCESS.logged,
  Component: EmployeeAddRoute,
};

export const basicPage = {
  path: '/basic',
  access: ACCESS.logged,
  Component: BasicPage,
  routes: [employeeListRoute, employeeAddRoute]
};

export const navigateTo = (nav, history) => {
  let navParams = {pathname: nav.path};
  navParams = nav.routeParams ? {...navParams, ...nav.routeParams} : navParams;
  history.push(navParams);
};

const routes = [basicPage, loginPage];

export default routes;



