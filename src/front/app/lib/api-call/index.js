export class ApiCallError extends Error {
  constructor(message, data) {
    super(message);

    this.name = 'ApiCallError';

    this.data = data;
  }
}

const stringifyQuery = (data) => Object.keys(data).filter((key) => data[key])
  .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`).join('&');

export const apiCall = ({method, url, data, query, headers}) => {
  if (!url) {
    return Promise.reject(new ApiCallError('api-call-error: url are required params'));
  }

  method = method || 'GET';
  headers = headers || {};
  headers['Content-type'] = headers['Content-type'] || 'application/json';

  if (query && method === 'GET') {
    url = `${url}?${stringifyQuery(query)}`;
  }

  if (data && typeof data === 'object' && headers['Content-type'] === 'application/json') {
    data = JSON.stringify(data);
  }

  return fetch(url, {method: method, headers: headers, body: data});
};