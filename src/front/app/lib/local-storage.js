
const getStorage = () => {
  const storage = global.localStorage;
  if (typeof storage === 'undefined') {
    throw new Error('Local storage do not supported');
  }
  return storage;
};

const serialize = (value) => JSON.stringify(value);

const deserialize = (value) => {
  if (typeof value !== 'string') {
    return;
  }
  try {
    return JSON.parse(value);
  } catch (e) {
    return value;
  }
};

export const set = (key, value) => {
  if (typeof value === 'undefined') {
    return remove(key);
  }

  getStorage().setItem(key, serialize(value));

  return value;
};

export const get = (key, defaultVal) => {
  const val = deserialize(getStorage().getItem(key));
  return (typeof val === 'undefined' ? defaultVal : val);
};

export const has = (key) => typeof get(key) !== 'undefined';

export const remove = (key) => getStorage().removeItem(key);

export const clear = () => getStorage().clear();

