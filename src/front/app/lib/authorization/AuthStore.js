import {get as getFromLocalStorage, set as setToLocalStorage} from '../local-storage';


export const setData = (authData) => setToLocalStorage('authorization', authData);

export const getData = () => getFromLocalStorage('authorization');

export const isAuthenticated = () => !!getData();
