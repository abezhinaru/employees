import React from 'react';
import PropTypes from 'prop-types';


export const withAuth = (Component) => {
  const C = (props, context) => {
    return (
      <Component {...props} auth={context.auth}/>
    )
  };


  C.contextTypes = { auth: PropTypes.object };

  return C;
};