import React, {Component, Children} from 'react';

import PropTypes from 'prop-types';

import { AuthStore } from "./AuthStore";

export class AuthStoreProvider extends Component {
  static childContextTypes = {
    auth: PropTypes.object
  };

  getChildContext() {
    return {
      auth: this.props.auth
    }
  }

  render() {
    return Children.only(this.props.children)
  }
}

