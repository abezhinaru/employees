import React, {Component} from 'react';
import environment from "../../../graphql-setup";

import EmployeesConteiner from './list/EmployeesConteiner';
import connectQueryRender from '../../../connectQueryRenderer';

import queryString from 'query-string';

import { withAuth } from '../../../lib/authorization/withAuth';


import {
  graphql,
  QueryRenderer
} from 'react-relay';


const query = graphql`
  query EmployeesRouteQuery ($orderBy: OrderByType, $count: Int) {
    viewer {
      ...EmployeesConteiner_viewer @arguments(orderBy: $orderBy, count: $count)
    }
  }
`;


export default withAuth(connectQueryRender({
  environment,
  query,
  mapPropsToVariables(props) {
    const {location} = props;
    const search = queryString.parse(location && location.search || '');
    return {
      count: search.count || 15,
      orderBy: {
        fieldName: 'createdAt',
        order: 'DESC'
      },
    };
  }
})(EmployeesConteiner));

