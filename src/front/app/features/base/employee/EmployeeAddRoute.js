import React, {Component} from 'react';
import environment from "../../../graphql-setup";

import EmployeeAddConteiner from './add/EmployeeAddConteiner';

import {
  graphql,
  QueryRenderer
} from 'react-relay';
import connectQueryRender from "../../../connectQueryRenderer";

const query = graphql`
  query EmployeeAddRouteQuery {
    viewer {
      id
      ...EmployeeAddConteiner_viewer
    }
  }
`;

export default connectQueryRender({
  environment,
  query,
})(EmployeeAddConteiner);
