// Higher Order Component
import React from 'react';
import {withFormik} from 'formik';
import yup from 'yup';


import {
  FormInput,
  FormDatePicker,
  FormSelect
} from '~/components/Form';

const mapRespValidationErroToFormikError = (validationErrorItems) => {
  return validationErrorItems.reduce((result, fieldError) => {
    result[fieldError.path] = fieldError.message;
    return result;
  }, {});
};

const EmployeeForm = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  setFieldValue
}) => (
  <form onSubmit={handleSubmit}>
    <FormInput
      name={'first_name'}
      type={'text'}
      value={values.first_name}
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder={'First Name'}
      errorText={touched.first_name && errors.first_name ? errors.first_name : null}
    /><br />

    <FormInput
      name={'last_name'}
      type={'text'}
      value={values.last_name}
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder={'Last Name'}
      errorText={touched.last_name && errors.last_name ? errors.last_name : null}
    /><br />

    <FormDatePicker
      name={'hire_date'}
      value={values.hire_date}
      onBlur={handleBlur}
      onChange={newData => {
        setFieldValue('hire_date', newData);
      }}
      placeholder={'Hire Date'}
    />

    <FormDatePicker
      name={'birth_date'}
      value={values.birth_date}
      onBlur={handleBlur}
      onChange={newData => {
        setFieldValue('birth_date', newData);
      }}
      placeholder={'Birthday'}
    />

    <FormSelect
      name={'gender'}
      options={[{value: 'M', label: 'Male'}, {value: 'F', label: 'Female'}]}
      onChange={value => {
        setFieldValue('gender', value)
      }}
      onBlur={handleBlur}
      errorText={touched.gender && errors.gender ? errors.gender : null}
      placeholder={'Gender'}
      value={values.gender}
    />

    <button type="submit" disabled={isSubmitting}>
      Submit
    </button>
  </form>
);

export default withFormik({
  mapPropsToValues: props => ({first_name: 'Some Name Andrey Andrey', last_name: 'Some Last Name Andrey Andrey', gender: 'M', birth_date: new Date(), hire_date: new Date()}),
  validationSchema()  {
    return yup.object().shape({
      first_name: yup.string().required('First Name is required'),
      last_name: yup.string().required('Last Name is required'),
      hire_date: yup.date().required('Hire date is required'),
      birth_date: yup.date().required('Birthday date is required'),
      gender: yup.string().oneOf(['M', 'F']).required('Gender is required'),
    });
  },
  handleSubmit(values, { props, setSubmitting, setErrors }) {
    props.onSubmitHandler(values)
      .catch((error) => {

        setErrors((mapRespValidationErroToFormikError(error.state)));

        setSubmitting(false);
      });
  },
})(EmployeeForm);

