import {
  graphql,
  commitMutation
} from 'react-relay';
import {
  ConnectionHandler
} from 'relay-runtime';

const mutation = graphql`
  mutation AddEmployeeMutation($input: AddEmployeeInput!) {
    addEmployee(input: $input) {
      employeeEdge {
        cursor,
        node {
          emp_no,
          birth_date,
          first_name,
          last_name,
          gender,
          hire_date
        }
      }
    }
  }
`;

const commit = ({
 environment,
 input,
 user
}) => {
  return new Promise((resolve, reject) => {
    commitMutation(environment, {
      mutation,
      variables: {
        input: input
      },
      onCompleted: (response, errors) => {
        if (errors) {
          reject(errors);
        } else {
          resolve && resolve(response);
        }
      },
      updater: (store) => {
        // const payload = store.getRootField('addEmployee');
        // const newEdge = payload.getLinkedRecord('employeeEdge');
        //
        // const userProxy = store.get(user.id);
        //
        // const conn = ConnectionHandler.getConnection(
        //   userProxy,
        //   'EmployeeTable_employees',
        // );
        //
        // ConnectionHandler.insertEdgeAfter(conn, newEdge);
      }
    });
  });
};

export default { commit };