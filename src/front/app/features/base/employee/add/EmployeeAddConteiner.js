import React, {Component} from 'react';
import {
  graphql,
  createFragmentContainer
} from 'react-relay';

import AddEmployeeMutation from './mutation/AddEmployeeMutation';


import {Section, SectionHeader, SectionContent} from '~/components/Section';
import EmployeeForm from './EmployeeForm';


import {employeeListRoute} from '~/routes';

const isValidationError = (error) => error.type === 'ValidationError';


class EmployeeAddContainer extends Component {

  moveToEmployees = () => {
    this.props.history.push({
      pathname: employeeListRoute.path,
    });
  };

  onSubmitHandler = (values) => {
    const {viewer, relay} = this.props;

    return AddEmployeeMutation.commit({
      environment: relay.environment,
      input: values,
      viewer,
    }).then(() => {
      this.moveToEmployees();
    }).catch((errors) => {
      const error = errors[0];

      if (isValidationError(error)) {
        return Promise.reject(error);
      }
      {
        //FIXME - catch another type of error
      }
    })
  };

  render() {
    return (
      <Section>
        <SectionContent>
          <EmployeeForm onSubmitHandler={this.onSubmitHandler}/>
        </SectionContent>
      </Section>
    );
  }
}

export default createFragmentContainer(
  EmployeeAddContainer, {
    viewer: graphql`
      fragment EmployeeAddConteiner_viewer on User {
        id
      }
    `
  }
);