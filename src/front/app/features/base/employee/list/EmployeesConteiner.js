import React from 'react';
import {
  graphql,
  createFragmentContainer,
  createPaginationContainer
} from 'react-relay';

import { withRouter } from 'react-router'

import queryString from 'query-string';

import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';

import EdgesTable from '~/components/EdgesTable';
import {Section, SectionHeader, SectionContent} from '~/components/Section';
import {FormSelect} from '~/components/Form';

import {employeeAddRoute} from '~/routes';


class EmployeesConteiner extends React.Component {

  constructor() {
    super();
    const search = queryString.parse(location && location.search || '');

    this.state = {
      tableStracture: [
        {key: 'first_name', label: 'First name'},
        {key: 'last_name', label: 'Last name'},
        {key: 'gender', label: 'Gender', converter: value => value === 'M' ? 'Male' : 'Female'},
        {key: 'hire_date', label: 'Hire date'},
        {key: 'birth_date', label: 'Birthday'},
      ],
      tableHeight: '765px',
      tableRowCount: search.count || 15
    };
  }


  moveToAddEmployee = () => {
    this.props.history.push({
      pathname: employeeAddRoute.path,
    });
  };

  refetch() {
    const {tableRowCount} = this.state;
    // we can pass new Query params
    const refetchVariables = {
      orderBy: {
        fieldName: 'createdAt',
        order: 'DESC'
      }
    };

    const errorHandler = error => console.log(error);

    this.props.relay.refetchConnection(tableRowCount, errorHandler, refetchVariables);
  }

  loadMore() {
    const {tableRowCount} = this.state;
    if (!this.props.relay.hasMore() || this.props.relay.isLoading()) return;

    const errorHandler = error => console.log(error);

    this.props.relay.loadMore(tableRowCount, errorHandler);
  }

  render() {
    const {viewer} = this.props;
    const {employees} = viewer;

    const {tableStracture, tableHeight} = this.state;

    return (

      <Section>
        <SectionHeader>
          <RaisedButton
            label="Add Employee"
            onClick={this.moveToAddEmployee}
          />
        </SectionHeader>
        <SectionContent>
          <EdgesTable height={tableHeight} edges={employees.edges} structure={tableStracture}/>

          <FlatButton onClick={() => this.loadMore()}
                      icon={<FontIcon className={'material-icons'}>hourglass_empty</FontIcon>} label="Load More"
                      primary={true}/>
          <FlatButton style={{float: 'right'}} onClick={() => this.refetch()}
                      icon={<FontIcon className={'material-icons'}>loop</FontIcon>} label="Reload" secondary={true}/>
        </SectionContent>
      </Section>
    );
  }
}

export default createPaginationContainer(
  withRouter(EmployeesConteiner), {
    viewer: graphql`
      fragment EmployeesConteiner_viewer on User
      @argumentDefinitions(
        count: {type: "Int"}
        cursor: {type: "String"}
        orderBy: {type: OrderByType})
      {
        id,
        employees (
          first: $count,
          after: $cursor
          orderBy: $orderBy
        ) @connection(key: "EmployeeTable_employees") {
          edges {
            node {
              id,
              emp_no,
              birth_date,
              first_name,
              last_name,
              gender,
              hire_date
            }
          }
        }
      }
    `
  }, {
    direction: 'forward',
    getConnectionFromProps(props) {
      return props.viewer && props.viewer.employees;
    },
    getVariables(props, {count, cursor, orderBy}, fragmentVariables) {
      return {
        count,
        cursor,
        orderBy: fragmentVariables.orderBy
      };
    },
    query: graphql`
      query EmployeesConteinerPaginationQuery(
      $count: Int!
      $cursor: String
      $orderBy: OrderByType
      ) {
        viewer {
          ...EmployeesConteiner_viewer @arguments(count: $count, cursor: $cursor, orderBy: $orderBy)
        }
      }
    `
  }
);