import React from 'react';
import {withRouter} from 'react-router';
import {Switch, Route, Redirect} from 'react-router-dom';
import {
  createFragmentContainer,
  graphql,
  QueryRenderer
} from 'react-relay';

import connectQueryRender from '~/connectQueryRenderer';
import environment from '~/graphql-setup';
import {employeeListRoute} from '~/routes';
import {Navigation, NavigationItem} from '~/components/Navigation';
import {MainContent} from '~/components/MainContent';
import {ApplyRoutes} from '~/components/Router';
import {PrivateRoute} from "../../components/Router/PrivateRoute";


const BaseRoute = ({history, routes}) => {
  const onNavigationClick = nav => {
    let navParams = {pathname: nav.path};
    navParams = nav.routeParams ? {...navParams, ...nav.routeParams} : navParams;
    history.push(navParams);
  };

  return (
    <div>
      <Navigation onClick={onNavigationClick} navs={[{label: 'Employee', ...employeeListRoute}]}/>
      <MainContent>
        <Switch>
          {routes && routes.map(route => {
            const RouteComponent = route.access === 'logged' ? PrivateRoute : Route;

            return <RouteComponent
              key={route.path}
              path={route.path}
              render={props => <route.Component {...props} routes={route.routes}/>}
              {...route.config}/>
          })}
          <Route render={() => <Redirect to={routes[0].path}/>}/>
        </Switch>
      </MainContent>
    </div>
  );
};


const BasicPageFragment = createFragmentContainer(withRouter(BaseRoute), {
    viewer: graphql`
      fragment BaseRoute_viewer on User {
        id
      }
    `
  }
);

const query = graphql`
  query BaseRouteQuery {
    viewer {
      ...BaseRoute_viewer
    }
  }
`;

export default connectQueryRender({
  environment,
  query,
})(BasicPageFragment);



