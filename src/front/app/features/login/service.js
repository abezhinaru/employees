import { apiCall } from '../../lib';

export const login = ({username, password}) => {
  return apiCall({
    method: 'POST',
    url: 'http://localhost:8080/api/auth',
    data: { username, password },
  }).then(resp => resp.json())
};