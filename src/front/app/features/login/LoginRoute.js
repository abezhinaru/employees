import React from 'react';

import {Redirect} from 'react-router-dom';

import LoginForm from './LoginForm';

import {withRouter} from 'react-router';
import {Section, SectionHeader, SectionContent} from '~/components/Section';
import {MainContent} from '~/components/MainContent';

import {withAuth} from '../../lib/authorization/withAuth';

import { login } from './service';

class Login extends React.Component {
  state = {redirectToReferrer: false};

  onSubmitHandler = (data) => {
    return login(data)
      .then(result => {
        if (result.status === 'error') {
          return Promise.reject(result);
        } else {
          const { data } = result;

          this.props.auth.setData(data);

          this.setState({
            redirectToReferrer: true
          })

        }
    })
  };

  render() {
    const {from} = this.props.location.state || {from: {pathname: "/"}};
    const {redirectToReferrer} = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from}/>;
    }

    return (
      <MainContent>
        <Section>
          <SectionHeader>
            You must log in to view the page at {from.pathname}
          </SectionHeader>

          <SectionContent>
            <LoginForm onSubmitHandler={this.onSubmitHandler}/>
          </SectionContent>
        </Section>
      </MainContent>
    );
  }
}

export default withRouter(withAuth(Login));
