import React from 'react';
import {withFormik} from 'formik';
import yup from 'yup';


import {
  FormInput,
  FormDatePicker,
  FormSelect
} from '~/components/Form';

const EmployeeForm = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting
}) => (
  <form onSubmit={handleSubmit}>
    <FormInput
      name={'username'}
      type={'text'}
      value={values.username}
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder={'Username'}
      errorText={touched.username && errors.username ? errors.username : null}
    /><br />

    <FormInput
      name={'password'}
      type={'password'}
      value={values.password}
      onBlur={handleBlur}
      onChange={handleChange}
      placeholder={'Password'}
      errorText={touched.password && errors.password ? errors.password : null}
    /><br />

    <button type="submit" disabled={isSubmitting}>
      Login
    </button>
  </form>
);

export default withFormik({
  mapPropsToValues: props => ({username: 'abezhinaru@gmail.com', password: 'qwerty'}),
  validationSchema()  {
    return yup.object().shape({
      username: yup.string().required('Username is required'),
      password: yup.string().required('Password is required'),
    });
  },
  handleSubmit(values, { props, setSubmitting, setErrors }) {
    props.onSubmitHandler(values)
      .catch(({messages}) => {

        const errors = messages.reduce((result, message) => {
          result[message.path] = message.message;
          return result;
        }, {});

        setErrors(errors);

        setSubmitting(false);
      });
  },
})(EmployeeForm);

