import React, {Component} from 'react';

import {
  graphql,
  QueryRenderer
} from 'react-relay';




export default ({environment, query, mapPropsToVariables}) => Component => {
  return componentProps => {
    return (
      <QueryRenderer
        environment={environment}
        query={query}
        variables={mapPropsToVariables ? mapPropsToVariables(componentProps): null}
        render={({error, props}) => {

          if (error) {
            return <div>Error!</div>;
          }

          if (props) {
            return <Component {...componentProps} {...props} />
          } else {
            return <div>Loading</div>;
          }
        }}
      />
    );
  }
}
