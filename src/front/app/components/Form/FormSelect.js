import React from 'react';
import PropTypes from 'prop-types';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


const FormSelect = ({
  placeholder,
  onChange,
  errorText,
  value,
  options,
  onBlur,
  name
}) => {
  return (
    <SelectField
      name={name}
      floatingLabelText={placeholder}
      value={value}
      errorText={errorText}
      onChange={(event, index) => {
        onChange(options[index].value)
      }}
      onBlur={onBlur}
    >
      { options && options.map(option =>
        <MenuItem key={option.value} value={option.value} primaryText={option.label} />
      )}
    </SelectField>
  );
};


FormSelect.propTypes = {
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.any,
  errorText: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  }))
};


export default FormSelect;