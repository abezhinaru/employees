import React from 'react';
import TextField from 'material-ui/TextField';
import PropTypes from 'prop-types';

const FormInput = ({
  errorText,
  type,
  name,
  value,
  onChange,
  onBlur,
  placeholder
}) => {
  return (
    <TextField
      errorText={errorText}
      type={type}
      name={name}
      onBlur={onBlur}
      value={value}
      onChange={onChange}
      floatingLabelText={placeholder}
    />
  );
};

FormInput.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.any,
  errorText: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func
};

export default FormInput;