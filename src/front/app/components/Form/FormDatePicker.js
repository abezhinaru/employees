import React from 'react';
import DatePicker from 'material-ui/DatePicker';


const FormDatePickerInput = ({
  placeholder,
  value,
  onChange,
  onBlur
}) => (
  <DatePicker
    name={name}
    floatingLabelText={placeholder}
    value={value}
    onChange={(event, date) => {
      onChange(date);
    }}
    onBlur={onBlur}
    container="inline"
  />
);

export default FormDatePickerInput;