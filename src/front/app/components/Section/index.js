import React, { Component } from 'react';

import './index.scss';


export const Section = ({children}) => (
  <section className={'custom-section'}>
    {children}
  </section>
);

export const SectionHeader = ({children}) => (
  <div className={'custom-section__header'}>
    {children}
  </div>
);

export const SectionContent = ({children}) => (
  <div className={'custom-section__content'}>
    {children}
  </div>
);


