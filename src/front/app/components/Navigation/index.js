import React, {Component} from 'react';
import MenuItem from 'material-ui/MenuItem';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';


export const Navigation = ({navs, onClick}) => {
  return (
    <Toolbar>
      <ToolbarGroup>
        {navs && navs.map((nav) =>
          <MenuItem
            key={nav.path}
            onClick={() => onClick && onClick(nav)}
          >
            {nav.label}
          </MenuItem>
        )}
      </ToolbarGroup>
    </Toolbar>
  );
};


// Navigation.contextTypes = {
//   history: PropTypes.object
// };
