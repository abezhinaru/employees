import React, { Component } from 'react';

import './index.scss';


export const MainContent = ({children}) => (
  <article className={'main-content'}>
    {children}
  </article>
);