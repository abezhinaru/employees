import React, {Component} from 'react';
import {graphql, createPaginationContainer} from 'react-relay';

import PropTypes from 'prop-types';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

const HeaderColumnLabel = ({label}) => <span>{label}</span>;

const TableHeaderRow = ({structure}) => (
  <TableRow>
    {structure && structure.map((item, index) => {
      return <TableHeaderColumn  key={index}>
        <HeaderColumnLabel label={item.label} />
      </TableHeaderColumn>
    })}
  </TableRow>
);

const TableBodyRow = ({structure, rowData}) => {
  return (
    <TableRow>
      {structure.map((item, index) => {
        const { key, converter } = item;

        let columnValue = rowData[key];
        columnValue = converter ? converter(columnValue) : columnValue;

        return <TableRowColumn key={index}>{columnValue}</TableRowColumn>
      })}
    </TableRow>
  )
};

const EdgesTable = ({
  edges,
  structure,
  adjustForCheckbox,
  displaySelectAll,
  fixedHeader,
  height
}) => (
  <Table height={height} fixedHeader={fixedHeader}>
    <TableHeader
      adjustForCheckbox={adjustForCheckbox}
      displaySelectAll={displaySelectAll}
    >
      <TableHeaderRow structure={structure} />
    </TableHeader>
    <TableBody>
      {edges && edges.map(edge => <TableBodyRow structure={structure} key={edge.node.id} rowData={edge.node}/>)}
    </TableBody>
  </Table>
);


EdgesTable.propTypes = {
  adjustForCheckbox: PropTypes.bool,
  displaySelectAll: PropTypes.bool,
  fixedHeader: PropTypes.bool,
  height: PropTypes.string,

  edges: PropTypes.arrayOf(PropTypes.shape({
    node: PropTypes.shape({
      id: PropTypes.string.isRequired
    }).isRequired
  }))
};

EdgesTable.defaultProps = {
  adjustForCheckbox: false,
  displaySelectAll: false,
  fixedHeader: true
};


export default EdgesTable;