import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {fakeAuth} from '../../lib/index';
import { loginPage } from '~/routes';
import { withAuth } from '../../lib/authorization/withAuth';

export const PrivateRoute = withAuth(({auth, render, component: Component, ...rest}) => {
  return <Route
    {...rest}
    render={(props) => {
      if (auth.isAuthenticated()) {
        return render ? render(props) : <Component {...props} />;
      } else {
        return ( <Redirect
          to={{
            pathname: loginPage.path,
            state: {from: props.location}
          }}
        />)
      }
    }}
  />
});

