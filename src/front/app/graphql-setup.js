import { Environment, Network, RecordSource, Store } from 'relay-runtime';

import {getData as getAuthData } from './lib/authorization/AuthStore';

const fetchQuery = (operation, variables) => {
  const headers = {
    'Content-Type': 'application/json'
  };

  const tokenData = getAuthData();

  if (tokenData) {
    headers['Authorization'] = `Bearer ${tokenData.token}`;
  }

  return fetch('/graphql', {
    method: 'POST',
    headers,
    body: JSON.stringify({
      query: operation.text,
      variables,
    }),
  }).then(response => response.json());
};



const environment = new Environment({
  network: Network.create(fetchQuery),
  store: new Store(new RecordSource())
});

export default environment;


//
// import { Environment, Network, RecordSource, Store } from 'relay-runtime';
//
// import {
//   RelayNetworkLayer,
//   urlMiddleware,
//   // batchMiddleware,
//   // loggerMiddleware,
//   // gqlErrorsMiddleware,
//   // perfMiddleware,
//   // retryMiddleware,
//   authMiddleware,
//   cacheMiddleware,
// } from 'react-relay-network-modern';
//
//
//
// const createEnvironment = () => {
//   const store = new Store(new RecordSource());
//
//   const network = new RelayNetworkLayer([
//     cacheMiddleware({
//       size: 100, // max 100 requests
//       ttl: 900000, // 15 minutes
//     }),
//     urlMiddleware({
//       url: () => Promise.resolve('/graphql'),
//     }),
//     // authMiddleware({
//     //   token: () => 'FUCK',
//     //   tokenRefreshPromise: (req) => {
//     //     console.log('[client.js] resolve token refresh', req);
//     //     return fetch('/jwt/refresh')
//     //       .then(res => res.json())
//     //       .then(json => {
//     //         const token = json.token;
//     //         store.set('jwt', token);
//     //         return token;
//     //       })
//     //       .catch(err => console.log('[client.js] ERROR can not refresh token', err));
//     //   },
//     // })
//   ]);
//
//   return new Environment({ network, store });
// };
//
//
// export default createEnvironment();