import 'regenerator-runtime/runtime';

import React from 'react';
import DOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { Authorization } from './lib';

import App from './App';

import routes from './routes';

const history = createHistory();

export default () => DOM.render(
  <MuiThemeProvider>
    <Authorization.AuthStoreProvider auth={Authorization.AuthStore}>
      <App history={history} routes={routes}/>
    </Authorization.AuthStoreProvider>
  </MuiThemeProvider>,
  document.getElementById('root')
);

