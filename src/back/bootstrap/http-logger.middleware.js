'use strict';

const fs     = require('fs');
const rfs    = require('rotating-file-stream');
const morgan = require('morgan');
const moment = require('moment');

const isDevelopment = process.env.NODE_ENV !== 'production';

module.exports = (server, {
  logDirectory,
  fileName,
  momentFormat,
  logFormat,
}) => {
  fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

  const accessLogStramSettings = { interval: '1d', path: logDirectory };
  const accessLogStream = rfs(fileName, accessLogStramSettings);

  morgan.token('server-time', () => moment().format(momentFormat));
  server.use(morgan(logFormat, {stream: accessLogStream}));

  isDevelopment && server.use(morgan(logFormat));
};
