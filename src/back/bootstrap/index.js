'use strict';

const bodyParser = require('body-parser');
const setupHTTPLogger = require('./http-logger.middleware');
const passport = require('passport');
const conf = require('../app-config');
const setupPasport = require('./passport');



module.exports = (server) => {
  server.use(bodyParser.json({limit: '50mb'}));
  server.use(bodyParser.urlencoded({extended: true}));
  server.use(bodyParser.text());
  server.use(passport.initialize());

  setupPasport();
  setupHTTPLogger(server, conf.morgan);
};