const passport = require('passport');
const BearerStrategy = require('passport-http-bearer').Strategy;
const jwt = require('jsonwebtoken');
const conf = require('../app-config');
const { Logger } = require('../lib');

module.exports = () => {
  const verify = (token, done) => {
    jwt.verify(token, conf.jwt.secret, (err, decoded) => {
      if (err) {
        Logger.error(err);
        return done(null, false, 'invalid e-mail address or password');
      }

      return done(null, {uuid: decoded.uuid}, {scope: 'all'})
    });
  };

  passport.use(new BearerStrategy(verify));

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });
};