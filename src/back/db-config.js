require('dotenv').config({ path: '../../.env' });

module.exports = {
  "development": {
    "port": 3306,
    "username": "root",
    "password": "password",
    "database": "employees",
    "host": "0.0.0.0",
    "dialect": "mysql",
    "operatorsAliases": false,
    "pool": {
      "max": 5,
      "min": 0,
      "acquire": 30000,
      "idle": 10000
    }
  },
  "production": {
    "port": 3306,
    // "username": process.env.DB_USERNAME,
    // "password": process.env.DB_PASSWORD,
    // "database": process.env.DB_DATABASE,
    // "host": process.env.DB_HOST,
    // "dialect": "mysql",
    "username": "root",
    "password": "password",
    "database": "employees",
    "host": "0.0.0.0",
    "dialect": "mysql",
    "operatorsAliases": false,
    "pool": {
      "max": 5,
      "min": 0,
      "acquire": 30000,
      "idle": 10000
    }
  }
};
