const bcrypt = require('bcrypt');

module.exports = {
  cryptString: string => bcrypt.hashSync(string, 10),
  compareString: (string, hash) => bcrypt.compare(string, hash)
};