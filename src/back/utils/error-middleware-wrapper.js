/**
 * Wrapper function that takes async middleware and
 * add ability to catch error and move it to the
 * next middleware by invoking next method
 *
 * @Example

 * const someCtrl = errorWrapper(async (req, res) => {
 *  throw new Error('test');
 * });
 *
 * app.use('/rote', someCtrl);
 *
 * app.use((error, req, res, next) => {
 *   error // true;
 * });
 *
 * @param fn - our middleware that should be wrapped
 * @returns {function(...[*])}
 */
module.exports = (fn) => (req, res, next) => fn(req, res, next).catch(next);