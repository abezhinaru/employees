'use strict';

const proxyquire = require('proxyquire');
const sinon = require('sinon');
const RespError = require('./resp-error');

describe('error-handler.middleware', () => {
  let res;
  let Logger;
  let sut;
  let next;

  beforeEach(() => {

    res = {
      send: sinon.spy(() => res),
      status: sinon.spy(() => res)
    };

    Logger = { error: sinon.spy() };

    next = sinon.spy();

    sut = proxyquire('./error-handler.middleware', {
      '../lib/index': {
        Logger
      },
    });

  });

  it('should handle error', () => {
    const error = new Error();

    sut(error, null , res, next);

    res.status.should.been.calledWith(500);

    res.send.should.been.calledWith({
      status: 'error',
      message: 'Internal server error'
    });

    Logger.error.should.been.calledWith(error);

    next.should.have.not.been.called;
  });

  it('should handle custom RespError', () => {
    const statusCode = 404;
    const message = 'Forbidden';

    sut(new RespError(statusCode, message), null , res, next);

    res.status.should.been.calledWith(statusCode);
    res.send.should.been.calledWith({
      status: 'error',
      message: message
    });

    next.should.have.not.been.called;
  });

  it('should call next when error is not provided', () => {
    sut(null, null, res, next);

    next.should.been.called;

    res.send.should.have.not.been.called;
    res.status.should.have.not.been.called;

    Logger.error.should.have.not.been.called;
  });


});