class RespounceError extends Error {
  constructor(httpStatusCode, messages) {
    super();

    this.httpStatusCode = httpStatusCode;
    this.messages = messages;
  }
}

module.exports = RespounceError;
