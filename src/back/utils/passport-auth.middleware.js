const passport  = require('passport');

const customAuthenticateMiddleware = (req, res, next) => {
  const checkAuthentication = (err, user, info) => {
    if (err) { return next(err); }
    if (!user) { return res.status(401).send({
      message: 'Unauthorized request'
    }) }
    req.login(user, function(err) {
      if (err) { return next(err); }
      next();
    });
  };

  passport.authenticate('bearer', { session: false }, checkAuthentication)(req, res, next);
};


module.exports = customAuthenticateMiddleware;