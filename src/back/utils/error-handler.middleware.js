const RespError = require('./resp-error');
const { Logger } = require('../lib/index');


module.exports = (error, req, res, next) => {
  let messages = [];

  if (!error) { return next(); }

  if (error instanceof RespError) {
    res.status(error.httpStatusCode);

    messages = error.messages;
  } else {
    res.status(500);

    messages.push({
      message: 'Internal server error'
    });

    Logger.error(error);
  }

  res
    .send({
      status: 'error',
      messages: messages
    });
};
