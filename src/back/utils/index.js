const { cryptString, compareString } = require('./crypt');

module.exports = {
  cryptString,
  compareString
};