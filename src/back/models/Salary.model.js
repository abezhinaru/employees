/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Salary = sequelize.define('Salary', {
    emp_no: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'employees',
        key: 'emp_no'
      }
    },
    salary: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    from_date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      primaryKey: true
    },
    to_date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    }
  }, {
    tableName: 'salaries'
  });


  Salary.associate = ({ Employee }) => {
    Salary.belongsTo(Employee, {
      foreignKey: 'emp_no'
    });
  };

  return Salary;
};
