'use strict';
/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Employee', {
    emp_no: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    birth_date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    first_name: {
      type: DataTypes.STRING(14),
      allowNull: false,
      validate: {
        len: {
          args: [1, 14],
          msg: 'Display name must be between 1 and 14 characters in length'
        }
      }
    },
    last_name: {
      type: DataTypes.STRING(16),
      allowNull: false,
      validate: {
        len: {
          args: [1, 16],
          msg: 'Display name must be between 1 and 14 characters in length'
        }
      }
    },
    gender: {
      type: DataTypes.ENUM('M','F'),
      allowNull: false
    },
    hire_date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    }
  }, {
    tableName: 'employees'
  });
};
