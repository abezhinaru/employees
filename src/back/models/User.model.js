'use strict';
/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('User', {
    uuid: {
      type : DataTypes.STRING,
      primaryKey: true,
      defaultValue: sequelize.UUIDV1
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    pswd: {
      type: DataTypes.STRING,
      allowNull: false
    }

  }, {
    tableName: 'users',
    timestamps: false
  });
};

