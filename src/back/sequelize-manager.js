'use strict';

const glob      = require('glob');
const path      = require('path');
const env         = process.env.NODE_ENV || 'development';

const Sequelize = require('sequelize');
const appConfig    = require('./app-config');
const sequelizeConf = require('./db-config')[env];

/**
 *
 * @param src - root directory path
 * @returns {*[]}
 */
const getDefinitionPaths = (src) => {
  const MODEL_PATTERN = path.join(src, '/**/*.model.js');
  const VIEW_PATTERN = path.join(src, '/**/*.view.js');

  return [
    ...glob.sync(MODEL_PATTERN),
    ...glob.sync(VIEW_PATTERN)
  ];
};

/**
 *
 * @param seqInst - instance of sequelize
 * @param acum    - result object
 * @param pathToDefinition - absolute path to definition
 * @returns {Object<Model>}
 */
const initDefinitionReducer = (seqInst, acum, pathToDefinition) => {
  const Definition = seqInst['import'](pathToDefinition);

  acum[Definition.name] = Definition;

  return acum;
};

/**
 *
 * @param database database name
 * @param username login username
 * @param password - login user password
 * @param conf - database configuration host, port etc.
 * @param src - src directory of project
 * @returns {{sequelize: Sequelize, Models: *|{}}}
 */
const init = (database, username, password, conf, src) => {
  const seqInst = new Sequelize(database, username, password, conf);

  const Models = getDefinitionPaths(src)
    .reduce(initDefinitionReducer.bind(void 0, seqInst), {});

  Object
    .keys(Models)
    .map((modelName) =>  Models[modelName])
    .forEach((Model) => {
      Model.associate && Model.associate(Models);
    });

  return { sequelize: seqInst, Models };
};

module.exports = init(
  sequelizeConf.database,
  sequelizeConf.username,
  sequelizeConf.password,
  sequelizeConf,
  appConfig.src
);