'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('salaries', [{
      emp_no: 10001,
      salary: 62102,
      from_date: '1987-06-26',
      to_date: '1988-06-25'
    }, {
      emp_no: 10002,
      salary: 66074,
      from_date: '1987-06-26',
      to_date: '1988-06-25'
    }, {
      emp_no: 10003,
      salary: 66596,
      from_date: '1987-06-26',
      to_date: '1988-06-25'
    }, {
      emp_no: 10004,
      salary: 66961,
      from_date: '1987-06-26',
      to_date: '1988-06-25'
    }, {
      emp_no: 10005,
      salary: 71046,
      from_date: '1987-06-26',
      to_date: '1988-06-25'
    }])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('salaries', null, {});
  }
};
