'use strict';

const { cryptString } = require('../utils');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('users', [{
      email: 'abezhinaru@gmail.com',
      pswd: cryptString('qwerty'),
    }])
  },

  down: (queryInterface) => {
    return queryInterface.bulkInsert('users', null, {});
  }
};
