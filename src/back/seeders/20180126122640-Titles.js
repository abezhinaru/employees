'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('titles', [{
        emp_no: 10002,
        title: 'Staff',
        from_date: '1996-08-03',
        to_date: '9999-01-01'
      }, {
        emp_no: 10003,
        title: 'Senior Engineer',
        from_date: '1996-08-03',
        to_date: '9999-01-01'
      }, {
        emp_no: 10004,
        title: 'Engineer',
        from_date: '1996-08-03',
        to_date: '9999-01-01'
      }, {
        emp_no: 10005,
        title: 'Senior Engineer',
        from_date: '1996-08-03',
        to_date: '9999-01-01'
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('titles', null, {});
  }
};
