'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('dept_manager', [{
      emp_no: 10002,
      dept_no: 'd001',
      from_date: '1985-01-01',
      to_date: '1991-10-01',
    }, {
      emp_no: 10003,
      dept_no: 'd002',
      from_date: '1985-01-01',
      to_date: '1991-10-01',
    }, {
      emp_no: 10004,
      dept_no: 'd003',
      from_date: '1985-01-01',
      to_date: '1991-10-01',
    }, {
      emp_no: 10005,
      dept_no: 'd004',
      from_date: '1985-01-01',
      to_date: '1991-10-01',
    }], {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('dept_manager', null, {});
  }
};
