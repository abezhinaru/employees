'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('employees', [{
      emp_no: 10001,
      birth_date: '1964-06-02',
      first_name: 'Andrey',
      last_name: 'Bezhinaru',
      gender: 'M',
      hire_date: '1994-07-01'
    }, {
      emp_no: 10002,
      birth_date: '1964-06-02',
      first_name: 'Bezalel',
      last_name: 'Simmel',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10003,
      birth_date: '1924-06-02',
      first_name: 'Parto',
      last_name: 'Bamford',
      gender: 'M',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10004,
      birth_date: '1964-06-02',
      first_name: 'Chirstian',
      last_name: 'Koblick',
      gender: 'M',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10005,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10006,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10007,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10008,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10009,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10010,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10011,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }, {
      emp_no: 10012,
      birth_date: '1964-06-02',
      first_name: 'Kyoichi',
      last_name: 'Maliniak',
      gender: 'F',
      hire_date: '1985-11-21'
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('employees', null, {});
  }
};
