'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('dept_emp', [{
      emp_no: 10002,
      dept_no: 'd007',
      from_date: '1996-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10003,
      dept_no: 'd004',
      from_date: '1995-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10004,
      dept_no: 'd004',
      from_date: '1994-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10005,
      dept_no: 'd003',
      from_date: '1993-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10006,
      dept_no: 'd005',
      from_date: '1992-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10007,
      dept_no: 'd008',
      from_date: '1991-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10008,
      dept_no: 'd005',
      from_date: '1990-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10009,
      dept_no: 'd006',
      from_date: '1989-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10010,
      dept_no: 'd004',
      from_date: '1988-08-03',
      to_date: '9999-01-01'
    }, {
      emp_no: 10011,
      dept_no: 'd006',
      from_date: '1987-08-03',
      to_date: '9999-01-01'
    }]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('dept_emp', null, {});
  }
};
