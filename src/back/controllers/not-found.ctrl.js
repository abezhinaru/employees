const RespError = require('../utils/resp-error');

module.exports = () => {
  throw new RespError(404, 'Route Not Found');
};
