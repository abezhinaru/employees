'use strict';

const { authorize } = require('./auth.service');

const wrap = require('../../utils/error-middleware-wrapper');

const authCtrl = wrap(async (req, res) => {
  const { username, password } = req.body;

  const data = await authorize(username, password);

  res.send({ status: 'ok', data });
});

module.exports = {
  auth: authCtrl
};
