'use strict';

const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const RespError = require('../../utils/resp-error');


describe('auth/service', () => {
  let res;
  let sut;
  let jsonwebtoken;
  let token = 'token';
  let Models;
  let employee;
  let errorResult;
  let tokenExpPerInMill = 86400;

  beforeEach(() => {
    employee = { emp_no: '1020213123' };

    errorResult = {
      status: 'error',
      message: 'wrong login or password'
    };

    Models = { Employee: { findOne: sinon.stub() } };

    jsonwebtoken = {
      sign: sinon.spy(() => token)
    };

    res = { send: sinon.spy() };


    sut = proxyquire('./auth.service', {
      jsonwebtoken,
      '../../app-config': {
        tokenExpPerInMill: tokenExpPerInMill,
        jwt: {
          secret: 'seecret'
        }
      },
      '../../sequelize-manager': {
        Models
      }
    });

  });

  it('should successfully authorized user and return token', () => {
    Models.Employee.findOne.returns(Promise.resolve(employee));

    return sut.authorize(
      'SamSmith',
      'bezhinaru'
    ).should.eventually.to.be.eql({ expiresIn: tokenExpPerInMill, token });
  });


  it('should return error due to wrong password', () => {
    Models.Employee.findOne.returns(Promise.resolve(employee));

    return sut.authorize(
      'SamSmith'
    ).should.be.rejectedWith(RespError, 'Wrong password. Please try again');
  });


  it('should return error due to wrong login', () => {
    Models.Employee.findOne.returns(Promise.resolve(null));

    return sut.authorize().should.be.rejectedWith(RespError, 'Wrong login. Please try again');


  });

});