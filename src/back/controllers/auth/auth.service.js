const jwt = require('jsonwebtoken');
const conf = require('../../app-config');
const {User} = require('../../sequelize-manager').Models;
const RespError = require('../../utils/resp-error');

const {compareString} = require('../../utils');


const createJWTToken = (uuid, jwtSeecret, expiresIn) => jwt.sign({uuid}, jwtSeecret, {expiresIn});

const authorize = async (username, password) => {
  username = username ? username.trim() : username;
  const user = await User.findOne({where: {email: username}});
  const errors = [];

  !user && errors.push({message: 'Wrong username. Please try again', path: 'username'});

  const isSamePassword = await compareString(password, user.pswd);

  !isSamePassword && errors.push({message: 'Wrong password. Please try again', path: 'password'});

  if (errors.length) {
    throw new RespError(200, errors);
  }

  const token = createJWTToken(user.uuid, conf.jwt.secret, conf.tokenExpPerInMill);

  return {expiresIn: conf.tokenExpPerInMill, token};
};

module.exports = {
  authorize
};



