'use strict';

const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

const RespError = require('../../utils/resp-error');


describe('auth/router', () => {
  let res;
  let req;
  let sut;
  let authorizeMock;
  let next;

  beforeEach(() => {
    res = {
      send: sinon.spy()
    };

    next = sinon.spy();

    authorizeMock = sinon.stub();

    req = {
      body: {
        username: 'Andrey',
        password: 'bezhinaru'
      }
    };

    sut = proxyquire('./auth.ctrl', {
      './auth.service': {
        authorize: authorizeMock
      }
    });
  });

  it('should authorize request', () => {
    const result = { token: 20 };

    authorizeMock.returns(Promise.resolve(result));

    return sut
      .auth(req, res, next)
      .then(() => {
        authorizeMock.should.been.calledWith(req.body.username, req.body.password);

        res.send.should.been.calledWith({status: 'ok', data: result });

        next.should.have.not.been.called;
      });
  });

  it('should handle authorization error', () => {
    const error = new RespError(200, 'message');

    authorizeMock.returns(Promise.reject(error));

    return sut
      .auth(req, res, next)
      .then(() => {
        authorizeMock.should.been.calledWith(req.body.username, req.body.password);

        res.send.should.have.not.been.called;

        next.should.been.calledWith(error);
      });
  });

});

