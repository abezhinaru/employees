'use strict';

const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');


describe('auth/controller', () => {
  let routes;
  let express;
  let authCtrl;

  beforeEach(() => {
    routes = { post: sinon.spy(() => routes) };

    authCtrl = {
      auth: 'smth'
    };

    express = {
      Router: sinon.stub().returns(routes)
    };

    proxyquire('./index', {
      express,
      './auth.ctrl': authCtrl
    })();
  });

  it('should register auth router', () => {
    const callArgs = routes.post.getCall(0).args;

    callArgs[0].should.equal('/');
    callArgs[1].should.equal(authCtrl.auth);
  });

});