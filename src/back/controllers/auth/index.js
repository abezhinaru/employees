const express = require('express');

const authCtrl = require('./auth.ctrl');

module.exports = () => express.Router()
  .post('/', authCtrl.auth);
