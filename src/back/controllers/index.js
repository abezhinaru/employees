'use strict';

const general   = require('./general');
const auth      = require('./auth');
const CRUDRoute = require('./crud-model');
const authMiddleware = require('../utils/passport-auth.middleware');
const { Models } = require('../sequelize-manager');

const errorHandlerMiddleware = require('../utils/error-handler.middleware');
const notFoundMiddleware = require('./not-found.ctrl');

const {
  Employee,
  Salary,
  Title,
  DeptManager,
  DeptEmp,
  Department
} = Models;

module.exports = app => {
  app.use('/api', general());
  app.use('/api/auth', auth());
  app.use('/api/employees', authMiddleware, CRUDRoute(Employee));
  app.use('/api/salaries', authMiddleware, CRUDRoute(Salary));
  app.use('/api/titles', authMiddleware, CRUDRoute(Title));
  app.use('/api/dept-manager', authMiddleware, CRUDRoute(DeptManager));
  app.use('/api/dept-emp', authMiddleware, CRUDRoute(DeptEmp));
  app.use('/api/departments', authMiddleware, CRUDRoute(Department));

  app.use('/api/*', notFoundMiddleware);
  app.use(errorHandlerMiddleware);
};
