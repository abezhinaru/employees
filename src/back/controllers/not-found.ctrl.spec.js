'use strict';
const { expect } = require('chai');

const sut = require('./not-found.ctrl');
const RespError = require('../utils/resp-error');

describe('not-found.middle', () => {
  it('should throw resp-error with 404 statuc code', () => {
    expect(() => {
      sut()
    }).to.throw(RespError, 'Route Not Found');
  });
});
