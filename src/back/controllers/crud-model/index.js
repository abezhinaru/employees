'use strict';

const express = require('express');
const crudModelCtrl = require('./crud-model.ctrl');

const createRouteParams = keys => keys.map(key => `:${key}`).join('/');

module.exports = Model => {
  const params = createRouteParams(Model.primaryKeyAttributes);

  return express.Router()
    .post(`/`, crudModelCtrl.create(Model))
    .put(`/${params}`, crudModelCtrl.update(Model))
    .delete(`/${params}`, crudModelCtrl.destroy(Model))
    .post(`/filter`, crudModelCtrl.filterBy(Model));
};
