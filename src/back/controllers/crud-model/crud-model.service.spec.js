'use strict';

const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();


describe('crud-model/service', () => {
  let Model;
  let sut;

  beforeEach(() => {
    Model = {
      update: sinon.spy(),
      findAll: sinon.spy(),
      create: sinon.spy(),
      destroy: sinon.spy()
    };

    sut = proxyquire('./crud-model.service', {
      '../../sequelize-manager': {
        Models: {
          MOCK_MODEL: {}
        }
      }
    });
  });

  it('#getByFilter method', () => {

    const filter = {
      limit: 10,
      offset: 10,
      with: ['MOCK_MODEL']
    };

    sut.getByFilter(Model, filter);

    Model.findAll.should.been.calledWith({ limit: 10, offset: 10, include: [ { model: {} } ] });
  });


  it('#getByFilter method with default arguments', () => {
    const filter = {};

    sut.getByFilter(Model, filter);

    Model.findAll.should.been.calledWith({ limit: 100, offset: 0, include: [] });
  });

  it('#create method', () => {
    const data = {};

    sut.create(Model, data);

    Model.create.should.been.calledWith(data);
  });

  it('#update method', () => {
    const data = {};
    const filterBy = {};

    sut.update(Model, data, filterBy);

    Model.update.should.been.calledWith(data, { where: filterBy });
  });

  it('#destroy metod', () => {
    const filterBy = {};

    sut.destroy(Model, filterBy);

    Model.destroy.should.been.calledWith({ where: filterBy });
  });
});