'use strict';

const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');


describe('crud-model/router', () => {
  let router;
  let express;
  let crudModelCtrl;
  let ModelMock

  let createSpy;
  let updateSpy;
  let deleteSpy;
  let selectSpy;

  beforeEach(() => {
    router = {
      post: sinon.spy(() => router),
      put: sinon.spy(() => router),
      delete: sinon.spy(() => router),
    };

    ModelMock = { primaryKeyAttributes: ['one', 'two'] };

    createSpy = sinon.spy();
    updateSpy = sinon.spy();
    deleteSpy = sinon.spy();
    selectSpy = sinon.spy();

    crudModelCtrl = {
      create: sinon.stub().returns(createSpy),
      update: sinon.stub().returns(updateSpy),
      destroy: sinon.stub().returns(deleteSpy),
      filterBy: sinon.stub().returns(selectSpy)
    };

    express = {
      Router: sinon.stub().returns(router)
    };

    proxyquire('./index', {
      express,
      './crud-model.ctrl': crudModelCtrl
    })(ModelMock);
  });

  it('should register create router', () => {
    router.post.getCall(0).should.been.calledWith('/', createSpy);

    crudModelCtrl.create.should.been.calledWith(ModelMock);
  });

  it('should register update router', () => {
    router.put.should.been.calledWith('/:one/:two', updateSpy);

    crudModelCtrl.update.should.been.calledWith(ModelMock);
  });

  it('should register delete router', () => {
    router.delete.should.been.calledWith('/:one/:two', deleteSpy);

    crudModelCtrl.destroy.should.been.calledWith(ModelMock);
  });

  it('should register filterBy router', () => {
    router.post.getCall(1).should.been.calledWith('/filter', selectSpy);

    crudModelCtrl.filterBy.should.been.calledWith(ModelMock);
  });

});