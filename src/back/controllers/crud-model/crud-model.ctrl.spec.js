'use strict';

const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');


describe('crud-model/controller', () => {
  let res;
  let req;
  let sut;
  let cms;
  let Model = {};
  let next;

  beforeEach(() => {
    res = { send: sinon.spy() };
    req = { body: {}};

    next = sinon.spy();

    cms = {
      getByFilter: sinon.stub(),
      create: sinon.stub(),
      update: sinon.stub(),
      destroy: sinon.stub()
    };

    sut = proxyquire('./crud-model.ctrl', { './crud-model.service': cms });
  });

  it('filterBy controller should been resolved with result', () => {
    const result = {};
    cms.getByFilter.returns(Promise.resolve(result));

    return sut.filterBy(Model)(req, res, next)
      .then(() => {
        cms.getByFilter.should.been.calledWith(Model, req.body);

        res.send.should.been.calledWith({ status: 'ok', data: result});

        next.should.have.not.been.called;
      });
  });

  it('filterBy controller should been rejected with error', () => {
    const error = new Error();

    cms.getByFilter.returns(Promise.reject(error));

    return sut.filterBy(Model)(req, res, next)
      .then(() => {
        cms.getByFilter.should.been.calledWith(Model, req.body);

        res.send.should.have.not.been.called;

        next.should.been.calledWith(error);
      });
  });

  it('update controller should update model', () => {
    let result = {};
    req = { body: { data: {} }, params: { one: 'one', two: 'two' } };

    cms.update.returns(Promise.resolve(result));

    return sut.update(Model)(req, res, next)
      .then(() => {
        cms.update.should.been.calledWith(Model, req.body.data, req.params);

        res.send.should.been.calledWith({ status: 'ok', data: result});
      });
  });

  it('update controller should been rejected', () => {
    const error = new Error();
    req = { body: { data: {} }, params: { one: 'one', two: 'two' } };

    cms.update.returns(Promise.reject(error));

    return sut.update(Model)(req, res, next)
      .then(() => {
        cms.update.should.been.calledWith(Model, req.body.data, req.params);

        res.send.should.have.not.been.called;

        next.should.been.calledWith(error);
      });
  });


  it('create controller should been resolved with result', () => {
    let result = {};
    req = { body: { data: {} } };

    cms.create.returns(Promise.resolve(result));

    return sut.create(Model)(req, res, next)
      .then(() => {
        cms.create.should.been.calledWith(Model, req.body.data);

        res.send.should.been.calledWith({ status: 'ok', data: result });

        next.should.have.not.been.called;
      });
  });

  it('create controller should been rejected with error', () => {
    const error = new Error();
    req = { body: { data: {} } };

    cms.create.returns(Promise.reject(error));

    return sut.create(Model)(req, res, next)
      .then(() => {
        cms.create.should.been.calledWith(Model, req.body.data);

        res.send.should.have.not.been.called;

        next.should.been.calledWith(error);
      });
  });

  it('destroy controller should been resoled', () => {
    let result = {};
    req = { params: { one: 'one', two: 'two' } };

    cms.destroy.returns(Promise.resolve(result));

    return sut.destroy(Model)(req, res, next)
      .then(() => {
        cms.destroy.should.been.calledWith(Model, req.params);

        res.send.should.been.calledWith({ status: 'ok', data: result });

        next.should.have.not.been.called;
      });
  });


  it('destroy controller should been rejected with error', () => {
    const error = new Error();
    req = { params: { one: 'one', two: 'two' }  };

    cms.destroy.returns(Promise.reject(error));

    return sut.destroy(Model)(req, res, next)
      .then(() => {
        cms.destroy.should.been.calledWith(Model, req.params);

        res.send.should.have.not.been.called;

        next.should.been.calledWith(error);
      });
  });


});