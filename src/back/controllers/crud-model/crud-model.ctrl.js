const {
  getByFilter,
  create,
  update,
  destroy
} = require('./crud-model.service');

const wrap = require('../../utils/error-middleware-wrapper');

const filterByCtrl = Model => wrap(async (req, res) => {
  const result = await getByFilter(Model, req.body);

  res.send({ status: 'ok', data: result });
});

const updateCtrl = Model => wrap(async (req, res) => {
  const { params, body } = req;
  const { data } = body;

  const result = await update(Model, data, params);

  res.send({ status: 'ok', data: result });
});

const createCtrl = Model => wrap(async (req, res) => {
  const { data } = req.body;

  const result = await create(Model, data);

  res.send({ status: 'ok', data: result });
});

const destroyCtrl = Model => wrap(async (req, res) => {
  const result = await destroy(Model, req.params);

  res.send({ status: 'ok', data: result });
});

module.exports = {
  filterBy: filterByCtrl,
  update: updateCtrl,
  create: createCtrl,
  destroy: destroyCtrl
};