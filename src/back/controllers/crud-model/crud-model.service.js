'use strict';

const { Models } = require('../../sequelize-manager');


const mapFilterToPredicate = (filterBy) => {
  const filter = {
    limit: filterBy.limit || 100,
    offset: filterBy.offset || 0,
    where: filterBy.where,
    order: filterBy.order
  };

  filter.include = filterBy.with ?
    filterBy.with.map(modelName => ({model: Models[modelName]})) :
    [];

  return filter;
};

/**
 * @param Model - sequelize module definition
 * @param {Object} filterBy [{}] - filter employees
 * @param {Number} filterBy.limit - items limit
 * @param {Number} filterBy.offset - items ofsset
 * @param {String[]} filterBy.with - which entities should be included to search result
 * @returns {Promise<T>}
 */
const getByFilter = (Model, filterBy = {}) =>
  Model.findAll(mapFilterToPredicate(filterBy));

/**
 * @param Model - sequelize module definition
 * @param object - module entity values
 * @returns {Promise<Model>}
 */
const create = (Model, object) => Model.create(object);


/**
 * @param Model - sequelize module definition
 * @param object - module entity values
 * @param filterBy - filter object to find which Models should be updated
 * @returns {Promise<Array>}
 */
const update = (Model, object, filterBy) => {
  return Model.update(object, {
    where: filterBy
  });
};

/**
 *
 * @param Model - sequelize module definition
 * @param filterBy - filter object to find which Models should be deleted
 * @returns {Promise<>}
 */
const destroy = (Model, filterBy) => {
  return Model.destroy({
    where: filterBy
  })
};

const findOne = (Model, primaryValues = {}) => {
  const primaryKeys = Model.primaryKeyAttributes;

  const where = Object.keys(primaryValues).reduce((result, key) => {
    if (primaryKeys.includes(key)) result[key] = primaryValues[key];

    return result;
  }, {});

  return Model.findOne({
    where
  })
};

const getByFilterAndCountAll = (Model, filterBy = {}) =>
  Model.findAndCountAll(mapFilterToPredicate(filterBy));


module.exports = { getByFilter, create, update, destroy, findOne, getByFilterAndCountAll };