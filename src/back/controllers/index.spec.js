'use strict';

const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');


describe('controllers/index', () => {
  let general;
  let auth;
  let CRUDRouteMock;
  let authMiddleware;
  let appMock;
  let stubCallResultMock = {};
  let notFoundMiddle = {};
  let Models; 

  beforeEach(() => {
    general = sinon.stub().returns(stubCallResultMock);
    auth = sinon.stub().returns(stubCallResultMock);
    CRUDRouteMock = sinon.stub().returns(stubCallResultMock);
    authMiddleware = {};
    appMock = { use: sinon.spy() };

    Models = {
      Employee: 'Employee',
      Salary: 'Salary',
      Title: 'Title',
      DeptManager: 'DeptManager',
      DeptEmp: 'DeptEmp',
      Department: 'Department'
    };

    proxyquire('./index', {
      './general': general,
      './auth': auth,
      './crud-model':  CRUDRouteMock,
      '../utils/passport-auth.middleware': authMiddleware,
      '../sequelize-manager': { Models },
      './not-found.ctrl': notFoundMiddle
    })(appMock);

  });

  it('should register general router', () => {
    appMock.use.should.been.calledWith('/', stubCallResultMock)
  });

  it('should register auth router', () => {
    appMock.use.should.been.calledWith('/api/auth', stubCallResultMock)
  });

  it('should register employees crud router', () => {
    CRUDRouteMock.should.been.calledWith(Models.Employee);

    appMock.use.should.been.calledWith('/api/employees', authMiddleware, stubCallResultMock)
  });

  it('should register salaries crud router', () => {
    CRUDRouteMock.should.been.calledWith(Models.Salary);

    appMock.use.should.been.calledWith('/api/salaries', authMiddleware, stubCallResultMock)
  });

  it('should register titles crud router', () => {
    CRUDRouteMock.should.been.calledWith(Models.Title);

    appMock.use.should.been.calledWith('/api/titles', authMiddleware, stubCallResultMock)
  });

  it('should register dept-manager crud router', () => {
    CRUDRouteMock.should.been.calledWith(Models.DeptManager);

    appMock.use.should.been.calledWith('/api/dept-manager', authMiddleware, stubCallResultMock)
  });

  it('should register dept-emp crud router', () => {
    CRUDRouteMock.should.been.calledWith(Models.DeptEmp);

    appMock.use.should.been.calledWith('/api/dept-emp', authMiddleware, stubCallResultMock)
  });

  it('should register departments crud router', () => {
    CRUDRouteMock.should.been.calledWith(Models.Department);

    appMock.use.should.been.calledWith('/api/departments', authMiddleware, stubCallResultMock)
  });


  it('should register page not found middleware', () => {
    CRUDRouteMock.should.been.calledWith(Models.Department);

    appMock.use.should.been.calledWith('*', notFoundMiddle);
  });


});