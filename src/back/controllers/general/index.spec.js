'use strict';

const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');


describe('general/router', () => {
  let router;
  let express;
  let generalCtrl;

  beforeEach(() => {
    router = {
      get: sinon.spy(() => router),
    };

    generalCtrl = {
      basePage: sinon.spy()
    };

    express = {
      Router: sinon.stub().returns(router)
    };

    proxyquire('./index', {
      express,
      './general.ctrl': generalCtrl
    })();
  });

  it('should register basePage route', () => {
    router.get.should.been.calledWith('/', generalCtrl.basePage);
  });


});