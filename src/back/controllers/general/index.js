'use strict';

const express = require('express');

const generalCtrl = require('./general.ctrl');

module.exports = () =>
  express.Router().get('/', generalCtrl.basePage);
