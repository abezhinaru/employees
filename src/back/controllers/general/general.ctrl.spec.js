'use strict';

const sinon = require('sinon');
const sut = require('./general.ctrl');


describe('general/controller', () => {
  let res;

  beforeEach(() => {
    res = { send: sinon.spy() };
  });


  it('should return base page', () => {
    sut.basePage(null, res);
    res.send.should.have.been.calledWith({status: 'ok'});
  });

});