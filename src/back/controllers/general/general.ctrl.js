'use strict';

const basePageCtrl = (req, resp) => {
  resp.send({status: 'ok'});
};

module.exports = {
  basePage: basePageCtrl
};