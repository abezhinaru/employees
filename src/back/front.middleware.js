'use strict';

const express = require('express');
const path = require('path');
const webpack = require('webpack');
const middleware = require('webpack-dev-middleware');
const layout = require('../../layout');
const isProduction = process.env.NODE_ENV === 'production';


const devFront = (app) => {
  const compiler = webpack(require('../../webpack/webpack.dev'));
  const md = middleware(compiler);

  app.use(md);

  app.get('*', (req, res) => {
    res.end(md.fileSystem.readFileSync(path.join(layout.dist.dir, 'index.html')));
  });
};


const prodFront = (app) => {
  const jsPattern = /\.js$/;
  app.use(express.static(layout.dist.dir));

  // replace .js to .js.gz and setup Content-Encoding
  app.use((req, res, next) => {
    if (jsPattern.test(req.url)) {
      req.url = req.url + '.gz';
      res.set('Content-Encoding', 'gzip');
    }

    next();
  });

  app.get('*', function(request, response) {
    response.sendFile(layout.dist.entries.html);
  });
};


module.exports = (app) => (isProduction ? prodFront : devFront)(app);