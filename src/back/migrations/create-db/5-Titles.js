'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('titles', {
      emp_no: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'employees',
          key: 'emp_no',
          onDelete: 'CASCADE'
        }
      },
      title: {
        type: Sequelize.STRING(50),
        allowNull: false,
        primaryKey: true
      },
      from_date: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        primaryKey: true
      },
      to_date: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('NOW')
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('NOW')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('titles');
  }
};