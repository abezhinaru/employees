'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('dept_emp', {
      emp_no: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'employees',
          key: 'emp_no',
          onDelete: 'CASCADE'
        }
      },
      dept_no: {
        type: Sequelize.CHAR(4),
        allowNull: false,
        primaryKey: true,
        references: {
          model: 'departments',
          key: 'dept_no',
          onDelete: 'CASCADE'
        }
      },
      from_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      to_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('NOW')
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('NOW')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('dept_emp');
  }
};