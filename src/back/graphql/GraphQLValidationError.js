const { GraphQLError } = require('graphql');

class GraphQLValidationError extends GraphQLError {

  static mapDBError(errors) {
    return errors.map((props) => GraphQLValidationError.createErrorItem(props));
  }

  static createErrorItem({message, path, type, value}) {
    return { message, path, type, value };
  }

  constructor(messages, message) {
    super(message || 'The request is invalid.');

    this.state = {
      type: 'ValidationError',
      messages
    };
  }
}


module.exports = GraphQLValidationError;