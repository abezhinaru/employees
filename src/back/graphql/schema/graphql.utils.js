const {
  offsetToCursor,
} = require('graphql-relay');

const cutomConnectionFromArray = (data, startOffset = 0, dataCount) => {
  const edges = data.map((value, index) => ({
    cursor: offsetToCursor(startOffset + index),
    node: value
  }));

  const firstEdge = edges[0];
  const lastEdge = edges[edges.length - 1];
  const hasNextPage = startOffset + edges.length < dataCount;
  const hasPreviousPage = startOffset !== 0;

  return {
    edges,
    pageInfo: {
      startCursor: firstEdge ? firstEdge.cursor : null,
      endCursor: lastEdge ? lastEdge.cursor : null,
      hasPreviousPage,
      hasNextPage,
    }
  };
};


module.exports = { cutomConnectionFromArray };