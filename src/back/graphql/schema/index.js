'use strict';
const {
  attributeFields
} = require('graphql-sequelize');

const Sequilize = require('sequelize');

const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLInputObjectType
} = require('graphql');

const {Logger} = require('../../lib/index');

const {
  connectionArgs,
  connectionDefinitions,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
  offsetToCursor,
  cursorToOffset
} = require('graphql-relay');

const GraphQLValidationError = require('../GraphQLValidationError');

const {
  Models
} = require('../../sequelize-manager');

const {
  Employee,
  User
} = Models;

const {
  getByFilterAndCountAll,
  findOne,
  create
} = require('../../controllers/crud-model/crud-model.service');

const MUtils = require('./model.utils');

const {cutomConnectionFromArray} = require('./graphql.utils');


const getRandomArbitrary = (min = 5000, max = 100000) => Math.round(Math.random() * (max - min) + min);

const {
  nodeInterface,
  nodeField
} = nodeDefinitions(
  globalId => {
    const {type, id} = fromGlobalId(globalId);
    const modelIdObj = MUtils.deserializeId(id);
    const Model = Models[type];

    if (Model) {
      return findOne(Model, modelIdObj);
    }

    return null;
  },
  obj => {
    if (obj instanceof Employee) {
      return GRaphQLEmployee;
    }

    if (obj instanceof User) {
      return GraphQLUser
    }

    return null;
  }
);

const GRaphQLEmployee = new GraphQLObjectType({
  name: 'Employee',
  fields: {
    ...attributeFields(Employee),
    id: globalIdField('Employee')
  },
  interfaces: [nodeInterface]
});

const {
  connectionType: EmployeesConnection,
  edgeType: GraphQLEmployeeEdge,
} = connectionDefinitions({
  name: 'Employee',
  nodeType: GRaphQLEmployee,
});

const GraphQLInputOrderByType = new GraphQLInputObjectType({
  name: 'OrderByType',
  fields: {
    fieldName: {type: GraphQLString},
    order: {type: GraphQLString}
  }
});

const GraphQLUser = new GraphQLObjectType({
  name: 'User',
  fields: {
    ...attributeFields(User),
    id: globalIdField('User'),
    employees: {
      type: EmployeesConnection,
      args: {
        orderBy: {type: GraphQLInputOrderByType},
        ...connectionArgs,
      },
      resolve: async (obj, args) => {
        const { orderBy } = args;
        const offset = args.after && cursorToOffset(args.after) + 1;

        const filter = { limit: args.first, offset };

        filter.order = orderBy ? [[orderBy.fieldName, orderBy.order]] : null;

        let {count, rows} = await getByFilterAndCountAll(Employee, filter);

        return cutomConnectionFromArray(MUtils.attachIdToEntities(Employee)(rows), offset, count);
      }
    }
  },
  interfaces: [nodeInterface]
});


const GraphQLAddEmployeeMutation = mutationWithClientMutationId({
  name: 'AddEmployee',
  inputFields: {
    birth_date: {
      type: GraphQLString
    },
    first_name: {
      type: GraphQLString
    },
    last_name: {
      type: GraphQLString
    },
    hire_date: {
      type: GraphQLString
    },
    gender: {
      type: GraphQLString
    }
  },
  outputFields: {
    employeeEdge: {
      type: GraphQLEmployeeEdge,
      resolve: async (employee) => {
        employee = MUtils.attachIdToEntity(Employee)(employee);

        return {
          cursor: offsetToCursor(100), // #FIXME should be right value
          node: employee,
        };
      }
    }
  },
  mutateAndGetPayload: (employeeData) => {
    const errors = [];
    // this check done only as example of backend validation
    if (employeeData.first_name.indexOf('Andrey') === -1) {
      errors.push(GraphQLValidationError.createErrorItem({
        message: 'First Name should contains \'Andrey\'. It\'s done only for example purpose',
        path: 'first_name',
        value: employeeData.first_name,
        type: 'Validation Error'
      }));
    }

    if (Object.keys(errors).length) {
      throw new GraphQLValidationError(errors, 'Error while validating user');
    }

    // FIXME Fix this shit
    employeeData.emp_no = getRandomArbitrary();

    return create(Employee, employeeData)
      .catch(error => {
        // sequilize error validation handler
        return new GraphQLValidationError(GraphQLValidationError.mapDBError(error.errors), 'Error while creating user');
      });
  }
});


const mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addEmployee: GraphQLAddEmployeeMutation,
  },
});


const query = new GraphQLObjectType({
  name: 'Query',
  fields: {
    node: nodeField,
    viewer: {
      type: GraphQLUser,
      args: {
        token: {type: GraphQLString},
      },
      resolve: async (obj, args, req) => {
        const { uuid } = req.user;
        const result = await findOne(User, {uuid});
        return MUtils.attachIdToEntity(User)(result);
      }
    }
  },
});

module.exports = new GraphQLSchema({
  mutation,
  query
});
