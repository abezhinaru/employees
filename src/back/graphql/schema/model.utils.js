


const serializeId = obj => JSON.stringify(obj);

const deserializeId = serializedObj => JSON.parse(serializedObj);

const generateIdObj = (primaryKeys, entity) => primaryKeys.reduce((result, key) => {
  result[key] = entity[key];

  return result;
}, {});

const attachIdToEntity = Model => entity => {
  const idObj = generateIdObj(Model.primaryKeyAttributes, entity);

  entity.id = serializeId(idObj);
  return entity;
};

const attachIdToEntities = Model => entities => entities.map(entity => attachIdToEntity(Model)(entity));



module.exports = {
  serializeId,
  deserializeId,
  generateIdObj,
  attachIdToEntity,
  attachIdToEntities
};