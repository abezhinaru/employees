'use strict';

const graphqlHTTP = require('express-graphql');
const schema = require('./schema');

const authMiddleware = require('../utils/passport-auth.middleware');

const formatError = ({message, originalError, locations, path}) => {
  return {
    message,
    locations,
    path,
    type: originalError && originalError.state && originalError.state.type,
    state: originalError && originalError.state && originalError.state.messages,
  }
};

module.exports = (app) => {
  const GQLMiddleware = graphqlHTTP({ formatError, graphiql: true, schema });

  app.use('/graphql', authMiddleware, GQLMiddleware);
};