'use strict';
const express = require('express');
// const path = require('path');
const conf = require('./app-config');
const appyRestAPIRoutes = require('./controllers');
const applyGraphQL = require('./graphql');
const applyFront = require('./front.middleware');
const bootstrap = require('./bootstrap');


const server = express();
bootstrap(server);

server.listen(conf.PORT, conf.HOST, () => {

  applyGraphQL(server);

  applyFront(server);

  appyRestAPIRoutes(server);

  console.log(`Application start on ${conf.PORT} ${conf.HOST}`);
});
