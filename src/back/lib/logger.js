'use strict';

const fs = require('fs');
const path = require('path');
const winston = require('winston');
const DailyRotateFileTransport = require('winston-daily-rotate-file');

const conf = require('../app-config');

const createLogger = (logDirPath, dailyRotateDatePattern) => {
  fs.existsSync(logDirPath) || fs.mkdirSync(logDirPath);

  return new (winston.Logger)({
      transports: [
        new (winston.transports.Console)({
          timestamp: () => (new Date()).toLocaleTimeString(),
          colorize: true,
          level: 'debug'
        }),
        new DailyRotateFileTransport({
          filename: path.join(logDirPath, '/log'),
          datePattern: dailyRotateDatePattern,
          prepend: true,
          level: 'info'
        }),
      ]
    });
};

module.exports = createLogger(conf.logDirPath, conf.winston.dailyRotateDatePattern);
