'use strict';

const path = require('path');

module.exports = {
  src: path.join(__dirname, ''),
  jwt: {
    secret: 'some_secret_code'
  },
  PORT: 8080,
  HOST: '0.0.0.0',
  logDirPath: path.join(__dirname, 'log'),
  morgan: {
    fileName: 'http-request.log',
    logDirectory: path.join(__dirname, 'log'),
    logFormat: '[:server-time] -- :method :remote-addr :url :status :res[content-length] - :response-time ms :remote-user [:date[clf]]',
    momentFormat: 'YYYY/MM/DD H:mm:ss'
  },
  winston: {
    dailyRotateDatePattern: 'appli-log.yyyy-MM-dd.'
  },
  tokenExpPerInMill: 86400 // 24 hours
};
