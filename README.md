# Employees examples

## How to firstly start app

```
npm install

docker-compose up

npm run sequelize:init-db // for db creating
npm run sequelize:run-all-seeders // for uploading test data

npm run start:dev
```