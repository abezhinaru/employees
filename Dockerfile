# FROM node:boron
FROM mhart/alpine-node:8

WORKDIR /usr/app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 8080

CMD ["npm", "start"]
