'use strict';

const
    chai = require('chai'),
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    chaiAsPromised = require("chai-as-promised");

global.env = null;
global.sinon = sinon;
chai.should();

chai.use(sinonChai);
chai.use(chaiAsPromised);