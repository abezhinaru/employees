#!/usr/bin/env bash
git clone https://github.com/datacharmer/test_db
cd test_db
mysql -uroot -ppassword -h 0.0.0.0 -P 3306 < employees.sql
exit;
rm -rf test_db