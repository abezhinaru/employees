const path = require('path');


const frontDir = path.resolve(__dirname, 'src/front');
const backDir = path.resolve(__dirname, 'src/back');
const distDir = path.resolve(__dirname, 'dist');


module.exports = {
  src: { backDir, frontDir },
  entries: {
    back: path.resolve(backDir, 'index.js'),
    front: {
      js: path.resolve(frontDir, 'index.js'),
      html: path.resolve(frontDir, 'index.html'),
    }
  },
  dist: {
    dir: distDir,
    entries: {
      html: path.resolve(distDir, 'index.html')
    }
  }
};