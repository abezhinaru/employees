'use strict';
const common = require('./webpack.common');
const merge = require('webpack-merge');
const layout = require('../layout');

const backAppConst = require('../src/back/app-config');

module.exports = merge(common, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: layout.dist.dir,
    proxy: {
      '/graphql': {
        target: `http://${backAppConst.HOST}:${backAppConst.PORT}`
      }
    },
    historyApiFallback: true
  }
});
