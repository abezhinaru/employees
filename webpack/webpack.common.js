'use strict';

const layout = require('../layout');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

const extractSass = new ExtractTextPlugin({
  filename: '[name].css',
  disable: !isProduction
});

module.exports = {
  entry: {
    app: layout.entries.front.js,
    vendor: [
      'react',
      'react-dom',
      'formik',
      'react-relay',
      'react-router-dom',
      'yup',
      'query-string',
      'history',
      'material-ui'
    ]
  },
  module: {
    rules: [
    //   {
    //   test: /\.js$/,
    //   loader: 'eslint-loader',
    //   enforce: 'pre',
    //   include: layout.src.frontDir,
    //   options: { formatter: require('eslint-friendly-formatter') }
    // },
      {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader'
      }
    }, {
      test: /\.scss$/,
      use: extractSass.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader'
        }, {
          loader: 'sass-loader'
        }]
      })
    }],
  },
  plugins: [
    extractSass,
    new HTMLWebpackPlugin({
      template: layout.entries.front.html
    }),
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendor' })
  ],
  output: {
    filename: '[name].bundle.js',
    path: layout.dist.dir,
    publicPath: '/'
  }
};
